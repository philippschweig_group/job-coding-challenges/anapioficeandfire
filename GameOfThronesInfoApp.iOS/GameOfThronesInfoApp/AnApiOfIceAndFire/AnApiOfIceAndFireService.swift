//
//  AnApiOfIceAndFireService.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation
import Siesta

class AnApiOfIceAndFireService: Service {
    
    init(baseUrl: String) {
        super.init(baseURL: baseUrl, standardTransformers: [.text, .image])
        
        self.configureTransformer("/houses") {
            try AnApiOfIceAndFireHouse.asListfromJson(json: $0.content)
        }
    }
    
    func houses() -> Resource { return resource("/houses") }
}

extension AnApiOfIceAndFireService {
    public static func create() -> AnApiOfIceAndFireService {
        return AnApiOfIceAndFireService(baseUrl: "https://anapioficeandfire.com/api")
    }
}
