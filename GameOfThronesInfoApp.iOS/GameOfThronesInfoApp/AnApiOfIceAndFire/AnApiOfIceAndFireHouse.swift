//
//  AnApiOfIceAndFireHouse.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

struct AnApiOfIceAndFireHouse: Codable {
    let url: String
    let name: String
    let region: String
    let coatOfArms: String
    let words: String
    let titles: [String]
    let seats: [String]
    let currentLord: String
    let heir: String
    let overlord: String
    let founded: String
    let founder: String
    let diedOut: String
    let ancestralWeapons: [String]
    let cadetBranches: [String]
    let swornMembers: [String]
}

extension AnApiOfIceAndFireHouse {
    static func asListfromJson(json: Data) throws -> [AnApiOfIceAndFireHouse] {
        let decoder = JSONDecoder()
        let parsed = try decoder.decode([AnApiOfIceAndFireHouse].self, from: json)
        return parsed
    }
}
