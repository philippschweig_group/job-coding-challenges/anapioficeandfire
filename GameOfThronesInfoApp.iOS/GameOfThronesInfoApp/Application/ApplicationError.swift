//
//  ApplicationError.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

protocol ApplicationErrorProtocol: Error, Equatable {
    var message: String { get }
}

struct ApplicationError: ApplicationErrorProtocol {
    let message: String
}
