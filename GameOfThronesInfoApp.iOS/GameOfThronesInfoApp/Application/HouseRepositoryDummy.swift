//
//  HouseRepositoryDummy.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

class HouseRepositoryDummy: HouseRepositoryProtocol {
    private let housesResult: HouseRepositoryResult<[House]>?
    
    init(housesResult: HouseRepositoryResult<[House]>?) {
        self.housesResult = housesResult
    }
    
    func houses(closure: @escaping HouseRepositoryHousesClosure) {
        guard let housesResult = self.housesResult else {
            return
        }
        closure(housesResult)
    }
    
    static func createSuccessful() -> HouseRepositoryDummy {
        let dummyHouses = [
            House(
                url: "url",
                name: "House 1",
                region: "region",
                currentLord: "currentLord",
                founded: "founded",
                founder: "founder"
            ),
            House(
                url: "url",
                name: "House 2",
                region: "region",
                currentLord: "currentLord",
                founded: "founded",
                founder: "founder"
            )
        ]
        
        return HouseRepositoryDummy(housesResult: HouseRepositoryResult.success(data: dummyHouses))
    }
    
    static func createLoading() -> HouseRepositoryDummy {
        return HouseRepositoryDummy(housesResult: nil)
    }
    
    static func createFailed() -> HouseRepositoryDummy {
        return HouseRepositoryDummy(
            housesResult: HouseRepositoryResult.failed(
                error: ApplicationError(message: "Something went wrong")
            )
        )
    }
}
