//
//  HouseRepository.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation
import Siesta

class HouseRepository: HouseRepositoryProtocol {
    
    private let service: AnApiOfIceAndFireService
    
    init(service: AnApiOfIceAndFireService) {
        self.service = service
    }
    
    func houses(closure: @escaping HouseRepositoryHousesClosure) {
        self.service.houses()
            .addObserver(owner: self, closure: { [weak self] resource, event in
                print(event)
                
                switch event {
                case .observerAdded, .requested:
                    // Do nothing
                    break
                case .newData(_), .notModified:
                    self?.handleHousesResponseNewData(newData: resource.latestData, closure: closure)
                case .requestCancelled, .error:
                    self?.handleHousesResponseError(closure: closure)
                }
            })
            .loadIfNeeded()
    }
    
    private func handleHousesResponseNewData(newData: Entity<Any>?, closure: HouseRepositoryHousesClosure) {
        guard let newData = newData?.content as? [AnApiOfIceAndFireHouse] else {
            closure(HouseRepositoryResult.failed(
                error: ApplicationError(message: "Loading houses failed. Please try again later.")
            ))
            return
        }
        
        let houses = AnApiOfIceAndFireModelMapper.mapHouses(anApiOfIceAndFireHouseList: newData)
        closure(HouseRepositoryResult.success(data: houses))
    }
    
    private func handleHousesResponseError(closure: HouseRepositoryHousesClosure) {
        closure(HouseRepositoryResult.failed(
            error: ApplicationError(message: "Loading houses failed. Please try again later.")
        ))
    }
    
}
