//
//  HouseDetailView.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import SwiftUI

struct HouseDetailView: View {
    var house: House
    
    var body: some View {
        List {
            Section(header: Text("Region")) {
                CustomTextView(text: house.region)
            }
            Section(header: Text("CurrentLord")) {
                CustomTextView(text: house.currentLord)
            }
            Section(header: Text("Founded")) {
                CustomTextView(text: house.founded)
            }
            Section(header: Text("Founder")) {
                CustomTextView(text: house.founder)
            }
        }
        .navigationTitle(house.name)
    }
}

fileprivate struct CustomTextView: View {
    var text: String
    
    var body: some View {
        if text.isEmpty {
            Text("---")
        }
        else {
            Text(text)
        }
    }
}

struct HouseDetailView_Previews: PreviewProvider {
    static var previews: some View {
        let example = House(
            url: "url content",
            name: "name content",
            region: "region content",
            currentLord: "currentLord content",
            founded: "founded content",
            founder: "founder content"
        )
        HouseDetailView(house: example)
    }
}
