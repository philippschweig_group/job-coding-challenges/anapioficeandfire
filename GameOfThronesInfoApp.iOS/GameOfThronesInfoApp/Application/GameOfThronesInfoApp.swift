//
//  GameOfThronesInfoApp.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//

import SwiftUI

@main
struct GameOfThronesInfoApp: App {
    var body: some Scene {
        WindowGroup {
            let service = AnApiOfIceAndFireService.create()
            let repository = HouseRepository(service: service)
            let viewModel = HouseListViewModel(repository: repository)
            HouseListView(viewModel: viewModel)
        }
    }
}
