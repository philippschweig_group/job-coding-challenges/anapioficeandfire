//
//  HouseListView.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//

import SwiftUI

struct HouseListView: View {
    @ObservedObject var viewModel: HouseListViewModel
    
    var body: some View {
        switch viewModel.state {
            case .idle:
                Text("Starting...").onAppear(perform: viewModel.load)
            case .loading:
                Text("Loading data...")
            case .failed(let error):
                LoadingFailedView(viewModel: viewModel, error: error)
            case .loaded(let houses):
                LoadingSucceededView(viewModel: viewModel, houses: houses)
        }
    }
}

fileprivate struct LoadingFailedView: View {
    let viewModel: HouseListViewModel
    let error: ApplicationError
    
    var body: some View {
        VStack(alignment: .center, spacing: 12) {
            Text(error.message)
                .multilineTextAlignment(.center)
            Button("Try again", action: viewModel.load)
        }
    }
}

fileprivate struct LoadingSucceededView: View {
    let viewModel: HouseListViewModel
    let houses: [House]
    
    var body: some View {
        NavigationView {
            VStack {
                List(houses) { house in
                    NavigationLink("\(house.name)", destination: viewModel.destinationForHouseSelection(house: house))
                }
                .navigationTitle("Houses")
            }
        }
    }
}

struct HouseListView_Previews: PreviewProvider {
    static var previews: some View {
        let viewModelSuccess = HouseListViewModel(repository: HouseRepositoryDummy.createSuccessful())
        HouseListView(viewModel: viewModelSuccess)
        
        let viewModelLoading = HouseListViewModel(repository: HouseRepositoryDummy.createLoading())
        HouseListView(viewModel: viewModelLoading)
        
        let viewModelFailed = HouseListViewModel(repository: HouseRepositoryDummy.createFailed())
        HouseListView(viewModel: viewModelFailed)
    }
}
