//
//  AnApiOfIceAndFireModelMapper.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

class AnApiOfIceAndFireModelMapper {
    
    static func mapHouses(anApiOfIceAndFireHouseList: [AnApiOfIceAndFireHouse]) -> [House] {
        return anApiOfIceAndFireHouseList.map { mapHouse(anApiOfIceAndFireHouse: $0) }
    }
    
    static func mapHouse(anApiOfIceAndFireHouse: AnApiOfIceAndFireHouse) -> House {
        return House(
            url: anApiOfIceAndFireHouse.url,
            name: anApiOfIceAndFireHouse.name,
            region: anApiOfIceAndFireHouse.region,
            currentLord: anApiOfIceAndFireHouse.currentLord,
            founded: anApiOfIceAndFireHouse.founded,
            founder: anApiOfIceAndFireHouse.founder
        )
    }
}
