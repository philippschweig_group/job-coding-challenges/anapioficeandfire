//
//  House.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

struct House: Codable, Identifiable, Equatable {
    var id = UUID()
    let url: String
    let name: String
    let region: String
    let currentLord: String
    let founded: String
    let founder: String
}
