//
//  HouseListViewModel.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 29.04.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation
import SwiftUI

class HouseListViewModel: ObservableObject {
    enum State {
        case idle
        case loading
        case failed(ApplicationError)
        case loaded([House])
    }

    @Published private(set) var state = State.idle
    
    private let repository: HouseRepositoryProtocol
    
    private var houses: [House] = []

    init(repository: HouseRepositoryProtocol) {
        self.repository = repository
    }

    func load() {
        self.state = .loading
        self.repository.houses(closure: self.handleHousesResult)
    }
    
    private func handleHousesResult(result: HouseRepositoryResult<[House]>) {
        switch result {
        case .success(data: let houses):
            self.houses = houses
            self.state = .loaded(self.houses)
        case .failed(error: let error):
            self.state = .failed(error)
        }
    }
    
    func destinationForHouseSelection(house: House) -> some View {
        return HouseDetailView(house: house)
    }
}
