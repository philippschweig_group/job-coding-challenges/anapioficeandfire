//
//  HouseRepositoryProtocol.swift
//  GameOfThronesInfoApp
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation

enum HouseRepositoryResult<Data> {
    case success(data: Data)
    case failed(error: ApplicationError)
}

typealias HouseRepositoryClosure<Data> = (HouseRepositoryResult<Data>) -> Void

protocol HouseRepositoryProtocol {
    typealias HouseRepositoryHousesClosure = HouseRepositoryClosure<[House]>
    func houses(closure: @escaping HouseRepositoryHousesClosure)
}
