//
//  HouseListViewModelTests.swift
//  GameOfThronesInfoAppTests
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation
import XCTest
@testable import GameOfThronesInfoApp

class HouseListViewModelTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoad_Success() throws {
        let expected = [
            House(
                url: "url",
                name: "House 1",
                region: "region",
                currentLord: "currentLord",
                founded: "founded",
                founder: "founder"
            )
        ]
        
        let repository = HouseRepositoryDummy(housesResult: HouseRepositoryResult.success(data: expected))
        let viewModel = HouseListViewModel(repository: repository)
        
        viewModel.load()
        
        guard case .loaded(let houses) = viewModel.state else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(expected, houses)
    }
    
    func testLoad_StayLoading() throws {
        let viewModel = HouseListViewModel(repository: HouseRepositoryDummy.createLoading())
        
        viewModel.load()
        
        guard case .loading = viewModel.state else {
            XCTFail()
            return
        }
        
        print("Success")
    }
    
    func testLoad_Error() throws {
        let expected = ApplicationError(message: "Load failed")
        let repository = HouseRepositoryDummy(housesResult: HouseRepositoryResult.failed(error: expected))
        let viewModel = HouseListViewModel(repository: repository)
        
        viewModel.load()
        
        guard case .failed(let error) = viewModel.state else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(expected, error)
    }
    
    func testDestinationForHouseSelection() throws {
        let viewModel = HouseListViewModel(repository: HouseRepositoryDummy.createSuccessful())
        
        viewModel.load()
        
        guard
            case .loaded(let houses) = viewModel.state,
            let firstHouse = houses.first
        else {
            XCTFail()
            return
        }
        
        let nextView = viewModel.destinationForHouseSelection(house: firstHouse)
        XCTAssertTrue(nextView is HouseDetailView)
    }

}
