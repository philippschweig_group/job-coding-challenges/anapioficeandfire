//
//  AnApiOfIceAndFireModelMapperTest.swift
//  GameOfThronesInfoAppTests
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import XCTest
@testable import GameOfThronesInfoApp

class AnApiOfIceAndFireModelMapperTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testMapHouses() throws {
        let apiHouses = [
            AnApiOfIceAndFireHouse(
                url: "url 1",
                name: "name 1",
                region: "region 1",
                coatOfArms: "coatOfArms 1",
                words: "words 1",
                titles: ["title 1"],
                seats: ["seat 1"],
                currentLord: "currentLord 1",
                heir: "heir 1",
                overlord: "overlord 1",
                founded: "founded 1",
                founder: "founder 1",
                diedOut: "diedOut 1",
                ancestralWeapons: ["ancestralWeapon 1"],
                cadetBranches: ["cadetBranch 1"],
                swornMembers: ["swornMember 1"]
            ),
            AnApiOfIceAndFireHouse(
                url: "url 2",
                name: "name 2",
                region: "region 2",
                coatOfArms: "coatOfArms 2",
                words: "words 2",
                titles: ["title 2"],
                seats: ["seat 2"],
                currentLord: "currentLord 2",
                heir: "heir 2",
                overlord: "overlord 2",
                founded: "founded 2",
                founder: "founder 2",
                diedOut: "diedOut 2",
                ancestralWeapons: ["ancestralWeapon 2"],
                cadetBranches: ["cadetBranch 2"],
                swornMembers: ["swornMember 2"]
            )
        ]
        
        let testUUID = UUID();
        var actual = AnApiOfIceAndFireModelMapper.mapHouses(anApiOfIceAndFireHouseList: apiHouses)
        actual = actual.map { house in
            var house = house
            house.id = testUUID
            return house
        }
        
        XCTAssertEqual(actual, [
            House(
                id: testUUID,
                url: "url 1",
                name: "name 1",
                region: "region 1",
                currentLord: "currentLord 1",
                founded: "founded 1",
                founder: "founder 1"
            ),
            House(
                id: testUUID,
                url: "url 2",
                name: "name 2",
                region: "region 2",
                currentLord: "currentLord 2",
                founded: "founded 2",
                founder: "founder 2"
            )
        ])
    }

}
