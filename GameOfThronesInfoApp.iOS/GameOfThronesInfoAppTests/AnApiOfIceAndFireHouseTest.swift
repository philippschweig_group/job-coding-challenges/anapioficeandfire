//
//  AnApiOfIceAndFireHouseTest.swift
//  GameOfThronesInfoAppTests
//
//  Created by Philipp Schweig on 01.05.22.
//  Copyright © 2022 Philipp Schweig. All rights reserved.
//

import Foundation
import XCTest
@testable import GameOfThronesInfoApp

class AnApiOfIceAndFireHouseTest: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAsListfromJson() throws {
        let jsonData = """
[
  {
    "url": "https://anapioficeandfire.com/api/houses/1",
    "name": "House Algood",
    "region": "The Westerlands",
    "coatOfArms": "A golden wreath, on a blue field with a gold border(Azure, a garland of laurel within a bordure or)",
    "words": "words",
    "titles": ["title 1"],
    "seats": ["seat 1"],
    "currentLord": "currentLord",
    "heir": "heir",
    "overlord": "https://anapioficeandfire.com/api/houses/229",
    "founded": "founded",
    "founder": "founder",
    "diedOut": "diedOut",
    "ancestralWeapons": [""],
    "cadetBranches": [],
    "swornMembers": []
  }
]
""".data(using: .utf8)
        
        guard
            let jsonData = jsonData,
            let apiHouse = try AnApiOfIceAndFireHouse.asListfromJson(json: jsonData).first
        else {
            XCTFail()
            return
        }
        
        XCTAssertEqual(apiHouse.url, "https://anapioficeandfire.com/api/houses/1")
        XCTAssertEqual(apiHouse.name, "House Algood")
        XCTAssertEqual(apiHouse.region, "The Westerlands")
        XCTAssertEqual(apiHouse.coatOfArms, "A golden wreath, on a blue field with a gold border(Azure, a garland of laurel within a bordure or)")
        XCTAssertEqual(apiHouse.words, "words")
        XCTAssertEqual(apiHouse.titles, ["title 1"])
        XCTAssertEqual(apiHouse.seats, ["seat 1"])
        XCTAssertEqual(apiHouse.currentLord, "currentLord")
        XCTAssertEqual(apiHouse.heir, "heir")
        XCTAssertEqual(apiHouse.overlord, "https://anapioficeandfire.com/api/houses/229")
        XCTAssertEqual(apiHouse.founded, "founded")
        XCTAssertEqual(apiHouse.founder, "founder")
        XCTAssertEqual(apiHouse.diedOut, "diedOut")
        XCTAssertEqual(apiHouse.ancestralWeapons, [""])
        XCTAssertEqual(apiHouse.cadetBranches, [])
        XCTAssertEqual(apiHouse.swornMembers, [])
    }

}
