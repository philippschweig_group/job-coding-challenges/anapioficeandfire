package com.anapioficeandfire

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface AnApiOfIceAndFireService {
    companion object {
        var BASE_URL = "https://anapioficeandfire.com/api/"

        fun create() : AnApiOfIceAndFireService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(AnApiOfIceAndFireService::class.java)
        }
    }

    @GET("houses")
    fun houses(): Call<List<House>>

    @GET("houses/{house}")
    fun houses(@Path("house") house: Int): Call<House>
}