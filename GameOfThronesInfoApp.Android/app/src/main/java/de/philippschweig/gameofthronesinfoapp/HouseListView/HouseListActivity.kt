package de.philippschweig.gameofthronesinfoapp.HouseListView

import android.os.Bundle
import android.view.View
import android.widget.AdapterView.OnItemClickListener
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.anapioficeandfire.AnApiOfIceAndFireService
import de.philippschweig.gameofthronesinfoapp.R

class HouseListActivity : AppCompatActivity() {
    lateinit var viewModel: HouseListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_house_list)

        viewModel = HouseListViewModel(AnApiOfIceAndFireService.create())

        viewModel.houses.observe(this) { houses ->
            if (houses == null) return@observe

            val houseListView = findViewById<View>(R.id.housesListView) as ListView
            houseListView.adapter = HouseListAdapter(this, houses)
        }

        val houseListView = findViewById<View>(R.id.housesListView) as ListView
        houseListView.onItemClickListener = OnItemClickListener { _, _, position, _ ->
            viewModel.onHouseSelected(this, position)
        }

        viewModel.loadHouses()
    }
}