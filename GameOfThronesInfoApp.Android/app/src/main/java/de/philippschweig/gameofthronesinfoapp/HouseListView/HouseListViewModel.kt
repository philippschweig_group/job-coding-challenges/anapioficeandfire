package de.philippschweig.gameofthronesinfoapp.HouseListView

import android.content.Context
import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.anapioficeandfire.AnApiOfIceAndFireService
import de.philippschweig.gameofthronesinfoapp.AnApiOfIceAndFireServiceModelMapper
import de.philippschweig.gameofthronesinfoapp.House
import de.philippschweig.gameofthronesinfoapp.HouseDetailView.HouseDetailActivity
import kotlinx.coroutines.*
import retrofit2.awaitResponse


class HouseListViewModel(private val anApiOfIceAndFireService: AnApiOfIceAndFireService) : ViewModel() {

    val loading = MutableLiveData<Boolean>()
    val errorMessage = MutableLiveData<String>()

    private var loadHousesJob: Job? = null
    private val _houses = MutableLiveData<List<House>?>()
    var houses: LiveData<List<House>?> = _houses

    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    fun loadHouses() {
        this.loadHousesJob = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = anApiOfIceAndFireService.houses().awaitResponse()

            withContext(Dispatchers.Main) {
                val responseBody = response.body()
                if (response.isSuccessful && responseBody != null) {
                    _houses.postValue(AnApiOfIceAndFireServiceModelMapper.mapHouses(responseBody))
                    loading.value = false
                } else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
    }

    fun onHouseSelected(context: Context, position: Int) {
        val intent = Intent(
            context.applicationContext,
            HouseDetailActivity::class.java
        )
        val house = _houses.value?.getOrNull(position) ?: return;
        intent.putExtra("house", house)
        context.startActivity(intent)
    }

    private fun onError(message: String) {
        errorMessage.value = message
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        loadHousesJob?.cancel()
    }
}