package de.philippschweig.gameofthronesinfoapp.HouseListView

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import de.philippschweig.gameofthronesinfoapp.House
import de.philippschweig.gameofthronesinfoapp.R


class HouseListAdapter(
    private val context: Context,
    private val houses: List<House>,
) : BaseAdapter() {

    override fun getCount(): Int {
        return houses.size
    }

    override fun getItem(position: Int): Any? {
        return houses.getOrNull(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var view: View? = convertView
        if (view == null) {
            val layoutInflater = LayoutInflater.from(context)
            view = layoutInflater.inflate(R.layout.adapter_house, parent, false)
            //val imageView: ImageView = view.findViewById(R.id.iconID) as ImageView
            val textView = view.findViewById(R.id.name) as TextView
            textView.text = houses[position].name
            //imageView.setImageResource(houses!![position].getIconID())
        }

        return view
    }
}