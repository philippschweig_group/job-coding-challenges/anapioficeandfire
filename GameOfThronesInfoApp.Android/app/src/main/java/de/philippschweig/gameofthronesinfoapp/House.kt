package de.philippschweig.gameofthronesinfoapp

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class House(
    val name: String,
    val region: String,
    val currentLord: String,
    val founded: String,
    val founder: String
) : Parcelable
