package de.philippschweig.gameofthronesinfoapp

class AnApiOfIceAndFireServiceModelMapper {
    companion object {
        fun mapHouses(houses: List<com.anapioficeandfire.House>) : List<House> {
            return houses.map { item -> mapHouse(item) }
        }

        fun mapHouse(house: com.anapioficeandfire.House) : House {
            return House(
                house.name,
                house.region,
                house.currentLord,
                house.founded,
                house.founder,
            )
        }
    }
}