package de.philippschweig.gameofthronesinfoapp.HouseDetailView

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import de.philippschweig.gameofthronesinfoapp.House
import de.philippschweig.gameofthronesinfoapp.R

class HouseDetailActivity : AppCompatActivity() {
    companion object {
        const val HOUSE_EXTRA = "house"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_house_detail)

        val house = intent.getParcelableExtra<House>(HOUSE_EXTRA) ?: return
        val viewModel = HouseDetailViewModel(house)

        title = viewModel.house.name
        findViewById<TextView>(R.id.region).text = computeText(viewModel.house.region)
        findViewById<TextView>(R.id.currentLord).text = computeText(viewModel.house.currentLord)
        findViewById<TextView>(R.id.founded).text = computeText(viewModel.house.founded)
        findViewById<TextView>(R.id.founder).text = computeText(viewModel.house.founder)
    }

    fun computeText(text: String): String {
        return text.ifEmpty {
            "---"
        }
    }
}