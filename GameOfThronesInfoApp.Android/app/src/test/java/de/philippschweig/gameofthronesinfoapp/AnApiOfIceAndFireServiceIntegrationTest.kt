package de.philippschweig.gameofthronesinfoapp

import com.anapioficeandfire.AnApiOfIceAndFireService
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import retrofit2.awaitResponse


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class AnApiOfIceAndFireServiceIntegrationTest {
    @Test
    fun houses() = runBlocking {
        val service = AnApiOfIceAndFireService.create()
        val response = service.houses().awaitResponse()
        val houses = response.body()

        print(houses)

        Assert.assertNotNull(houses)
        Assert.assertEquals(houses?.size, 10)
    }

    @Test
    fun house() = runBlocking {
        val service = AnApiOfIceAndFireService.create()
        val response = service.houses(1).awaitResponse()
        val house = response.body()

        print(house)

        Assert.assertNotNull(house)
    }
}