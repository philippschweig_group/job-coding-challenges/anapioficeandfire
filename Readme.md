# A simple app for *An API of Ice And Fire*

This is a sample app to show information from the *API of Ice And Fire*.

> An API of Ice And Fire is the world's greatest source for quantified and structured data from the universe of Ice and Fire (and the HBO series Game of Thrones). We give you access to data about all the Books, Characters and Houses in an easy to use JSON format.
>
> -- <cite>[Joakim Skoog](https://anapioficeandfire.com/About)</cite>

More about *An API of Ice And Fire*: https://anapioficeandfire.com/

## Screenshots (iOS)

<img src="screenshots/ios.houses.png" alt="iOS Houses list view" width="400"/>
<img src="screenshots/ios.house.png" alt="iOS House detail view" width="400"/>
